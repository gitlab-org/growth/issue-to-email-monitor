# Issue to email monitor

This monitor determines if there are issues observing from the time a GitLab issue is created to the time an email about the issue is received.

This allows us to know how long the emails take to be delivered as this can impact user satifaction based on the timeliness of receiving these updates.  Many of the same components are used as a part of new user registration, so this monitor is also a good proxy for understanding if new users are impacted by email delays as it pertains to their new account registration email.

# How does it work?

1. Creates an issue in a specific project in GitLab that is configured to email a specific GitLab user with a gmail.com email address
1. Polls periodically via pop that gmail address looking for the email for that issue
1. Downloads all emails in the inbox and deletes them
1. If it finds the email within 300 seconds, it declares success
1. If it doesn't, it declares failure and gives up
1. Deletes issue it created so as to not waste storage resources

The program logs all status changes and details as it operates in a local log file and sends some messages to slack.

# Slack notifications

When the email takes longer than 300 seconds to be received, more than 3 times sequentially, the Slack channel [#issue_to_email_monitor](https://gitlab.slack.com/archives/C02LG9YAVRN) is notified.

Example: `Potential incident!  Consider escalating to infrastructure. 3 sequential failure of messages to be received within 300 seconds`

It also notifies the channel when it recovers so we know the incident is over.

Example:
`Good news: Email delivery has recovered`

It also gives the channel an FYI with each message isn't received in time, with enough information to search for that email to be researched (such as searching for subject line in mailgun)

Example:
`FYI: Email for issue 9769 with subject line "Issue create UTC 2021-11-09 17:47:29.257210" failed to be received before timeout of 2 seconds.  1 sequential failures`

# How to research an issue with email generation and delivery

(Draft)

1. Check the GitLab sidekiq queues to see if they are backed up
1. Check the mailgun status page and the gmail status spage to see if there are known issues with either
1. Login to mailgun and find the message in question to research when the received it and when gmail accepted it
1. Escalate to mailgun via support ticket (within 3 days of the email being sent as they only have detailed logs for 3 days)

# What is the path of a GitLab issue email

1. Issue created in GitLab
1. GitLab sends email to mailgun
1. Mailgun sends email to user's email provider

# Limitations

* Does not monitor non-issue create emails (though other GitLab generated emails use most of the same components)
* Does not use an email provider other than gmail

# How to install and configure

TBD

# Example logs

## Success

```
2021-11-02 18:53:39.214872: issue_create_to_email_monitor: Issue created - issueid = 5503
2021-11-02 18:53:54.230463: issue_create_to_email_monitor: Download email attempt - total wait time 15
2021-11-02 18:53:55.754436: issue_create_to_email_monitor: Email: (b'+OK message follows', [b'Delivered-To: testmemailexample@gmail.com', b'Received: by....rest of the email
2021-11-02 18:53:55.754919: issue_create_to_email_monitor: Expected email found
2021-11-02 18:53:56.143791: issue_create_to_email_monitor: Email received in 15 seconds
2021-11-02 18:53:56.461020: issue_create_to_email_monitor: Issue delete result: <Response [204]>
2021-11-02 18:53:56.461371: issue_create_to_email_monitor: Sleeping 120 seconds before next attempt
```

## Failure
```
2021-11-02 01:08:40.375763: issue_create_to_email_monitor: Email failed to be received before timeout
```


