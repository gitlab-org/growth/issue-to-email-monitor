#!/usr/bin/python3

import poplib
import time
from datetime import datetime
import os
import requests
from slack_sdk.webhook import WebhookClient


failures=0
sequentialfailuresbeforeescalation=3
failure_escalated=False

def slack(msg):

    slacksent=False
    try:
        webhook = WebhookClient(os.environ.get('SLACKWEBHOOK'))
        response = webhook.send(text=msg)
        slacksent=True
    finally:
        slacksent=False


def log(line):
    now=str(datetime.utcnow())
    f = open("pop.out", "a")
    f.write(f'{now}: issue_create_to_email_monitor: {line}\n')
    f.close()


def monitor_attempt():
    global failures
    global sequentialfailuresbeforeescalation
    global failure_escalated

    # create issue

    headers = {
        'PRIVATE-TOKEN':os.environ.get('GITLABTOKEN'),
    }

    issuecreated=str(datetime.utcnow())

    params = (
        ('title', f'Issue create UTC {issuecreated}'),
    )

    issueid = 0

    try:
        response = requests.post('https://gitlab.com/api/v4/projects/30658244/issues', headers=headers, params=params)
        issueid = response.json()['iid']
    finally:
        if (issueid==0):
            log('Could not create issue')
            return()

    senttime=datetime.now()

    log(f'Issue created - issueid = {issueid}')

    foundMessage=False
    secondstoreceive=0
    maxwaitseconds=300
    sleepbetweenpopchecks=2

    while ((not foundMessage) and (secondstoreceive<maxwaitseconds)):
        time.sleep(sleepbetweenpopchecks)
        sleepbetweenpopchecks=sleepbetweenpopchecks*1.5
        receivedtime=datetime.now()
        secondstoreceive=int((receivedtime-senttime).total_seconds())
        log(f'Download email attempt - total wait time {secondstoreceive}')
        try:
            M = poplib.POP3_SSL('pop.gmail.com',995,timeout=30)
            M.user('waynegitlabtest')
            M.pass_(os.environ.get('GMAILPASSWORD'))
            numMessages = len(M.list()[1])
            if (numMessages==0):
                 M.quit()
        finally:
            log(f'Emails found: {numMessages}')


        if (numMessages>0):
            for i in range(numMessages):
                try:
                    toplines=M.top(i+1,1) 
                finally:
                    log(f'Email {i} processed')
                    
#                log(f'Email: {toplines}')
                if (str(toplines).find(f'Issue create UTC {issuecreated}') != -1):
                    foundMessage=True
                    log('Expected email found')
                else :
                    log('This is not the email we are looking for')
                M.dele(i+1)
            M.quit()

    if (foundMessage) :
        log(f'Email received in {secondstoreceive} seconds')
        failures=0
    else :
        failures=failures + 1
        log(f'Email failed to be received before timeout of {maxwaitseconds} seconds.  {failures} sequential failures')
        slack(f'FYI: Email for issue {issueid} with subject line "Issue create UTC {issuecreated}" failed to be received before timeout of {maxwaitseconds} seconds.  {failures} sequential failures')


    # delete the issue

    try:
        response = requests.delete(f'https://gitlab.com/api/v4/projects/30658244/issues/{issueid}', headers=headers)
    finally:
        log(f'Issue delete result: {response}')

   # check for declaring outage over if message was found and we are in an outage
    if (failure_escalated and foundMessage) :
        log(f'WARN Email delivery has recovered')
        slack(f'Good news: Email delivery has recovered')
        failure_escalated=False

   # escalate if necessary

    if (failures>=sequentialfailuresbeforeescalation) :
       if (not failure_escalated) :
           log(f'WARN Escalating due to {failures} sequential falures')
           slack(f'Potential incident!  Consider escalating to infrastructure. {failures} sequential falure of messages to be received within {maxwaitseconds} seconds')
           failure_escalated=True
       else :
           log(f'Additional sequential failure #{failures} - not escalating')


sleepbetweenattempts=120

while True:
    try:
        monitor_attempt()
        log(f'Sleeping {sleepbetweenattempts} seconds before next attempt')
    finally:
        time.sleep(sleepbetweenattempts)

